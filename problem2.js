/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

module.exports = function problem2(filePath, callback) {
    if (typeof callback != 'function') {
        console.error(new Error('callback is not a function!'));
    }
    else if (typeof filePath != 'string') {
        callback(new Error('filePath is should be a string'));
    }
    else {
        const filenamesPath = path.join(__dirname, './output/filenames.txt');

        fs.readFile(filePath, 'utf-8', (error, content) => {
            if (error) {
                callback(error);
            }
            else {
                const contentInUpperCase = content.toUpperCase();
                const fileNameUpperCase = 'inUpperCase.txt';
                const fileUpperCasePath = path.join(__dirname, './output/', fileNameUpperCase);

                fs.writeFile(filenamesPath, fileNameUpperCase, (error) => {
                    if (error) {
                        callback(error);
                    }
                    else {
                        fs.writeFile(fileUpperCasePath, contentInUpperCase, (error) => {
                            if (error) {
                                callback(error);
                            }
                            else {
                                fs.readFile(fileUpperCasePath, 'utf-8', (errorm, upperCaseContent) => {
                                    if (error) {
                                        callback(error);
                                    }
                                    else {
                                        let contentInLowerCase = upperCaseContent.toLowerCase();
                                        let sentences = contentInLowerCase.split('.');
                                        let sentencesFileName = 'sentences.txt';
                                        const sentencesFilePath = path.join(__dirname, './output/', sentencesFileName);

                                        fs.writeFile(sentencesFilePath, JSON.stringify(sentences), (error) => {
                                            if (error) {
                                                callback(error);
                                            }
                                            else {
                                                fs.appendFile(filenamesPath, '\n' + sentencesFileName, (error) => {
                                                    if (error) {
                                                        callback(error);
                                                    }
                                                    else {
                                                        fs.readFile(sentencesFilePath, 'utf-8', (error, sentences) => {
                                                            if (error) {
                                                                callback(error);
                                                            }
                                                            else {
                                                                try {
                                                                    const sortFileName = 'sorted.text';
                                                                    const sortFilePath = path.join(__dirname, './output/', sortFileName);
                                                                    const sentencesArray = JSON.parse(sentences);

                                                                    sentencesArray.sort();
                                                                    fs.writeFile(sortFilePath, JSON.stringify(sentencesArray), (error) => {
                                                                        if (error) {
                                                                            callback(error);
                                                                        }
                                                                        else {
                                                                            fs.appendFile(filenamesPath, '\n' + sortFileName, (error) => {
                                                                                if (error) {
                                                                                    callback(error);
                                                                                } else {
                                                                                    fs.readFile(filenamesPath, 'utf-8', (error, data) => {
                                                                                        if (error) {
                                                                                            callback(error);
                                                                                        }
                                                                                        else {
                                                                                            let fileNames = data.split('\n');
                                                                                            fileNames.forEach(file => {
                                                                                                let filePath = path.join(__dirname, './output/', file);
                                                                                                fs.unlink(filePath, (error) => {
                                                                                                    if (error) {
                                                                                                        callback(error);
                                                                                                    }
                                                                                                    else {
                                                                                                        callback(null, "Done");
                                                                                                    }
                                                                                                })
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            });
                                                                        }
                                                                    })
                                                                } catch (error) {
                                                                    callback(error);
                                                                }
                                                            }
                                                        })
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
}