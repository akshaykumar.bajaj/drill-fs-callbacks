const path = require('path');
const testProblem1 = require('../problem1');

let newDirPath = path.join(__dirname, '../output/new');

//Check for Success
testProblem1(newDirPath, (error, success) => {
    if (error) {
        console.error("Failed:", error);
    } else {
        console.log("success:", success);
    }
})

//Check for Fails

//1.No parameters passed
testProblem1();

//2. Callback funtion not passed
testProblem1(newDirPath)

//3.Directory path not passed as string
testProblem1(234, (error, success) => {
    if (error) {
        console.error("Failed:", error);
    } else {
        console.log("success:", success);
    }
})
