const path = require('path');
const testProblem2 = require('../problem2');

let filePath = path.join(__dirname, '../input/lipsum.txt');

//Check for Success
testProblem2(filePath, (error, success) => {
    if (error) {
        console.error("Failed:", error);
    } else {
        console.log("success:", success);
    }
})

//Check for Fails

//1.No parameters passed
testProblem2();

//2. Callback funtion not passed
testProblem2(filePath)

//3.Directory path not passed as string
testProblem2(234, (error, success) => {
    if (error) {
        console.error("Failed:", error);
    } else {
        console.log("success:", success);
    }
})
