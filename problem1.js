/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');
const path = require('path');
module.exports = function problem1(newDirPath, callback) {
    if(typeof callback != 'function'){
        console.error(new Error ('callback is not a function!'));
    }
    else if(typeof newDirPath != 'string'){
        callback(new Error('newDirPath is should be a string'));
    }
    else{
    fs.mkdir(newDirPath, (error) => {
        if (error) {
            callback(error);
        }
        else {
            for (let fileNumber = 1; fileNumber < 10; fileNumber += 1) {
                let fileName = 'newFile' + fileNumber + '.json'
                let filePath = path.join(newDirPath, fileName);
                let content = { 'name': fileName };

                fs.writeFile(filePath, JSON.stringify(content), (error) => {
                    if (error) {
                        callback(error);
                    } else {
                        fs.unlink(filePath, (error) => {
                            if (error) {
                                callback(error);
                            } else {
                                callback(null, "Done" + fileNumber);
                            }
                        })
                    }
                })
            }
        }
    })
}
}